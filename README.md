Instructiuons

- Download Zip file
- Extract contents to Windows drive C: (after extraction the folder "C:\LetsEncrypt" should exist)
- Edit file lego.ps1

Changes to do:

$Env:RFC2136_TSIG_KEY="KEYNAME"                     - Modify "KEYNAME" to used RFC2136 key name
$Env:RFC2136_TSIG_SECRET="VERYLONGANDSECRETKEY"     - Modify "VERYLONGANDSECRETKEY" to used key secret

##### Anpassen ####
$EMAIL="email.adress@uni-bielefeld.de"              - Edit email address
$SERVICENAME="servicename"                          - Change name according to service
$DOMAIN="domainname.uni-bielefeld.de"               - Change domain
$BASEDIR="C:\LetsEncrypt"
$PKCS12PW="CERTPASSWORD"                            - Edit certificate password
##### Anpassen ende ####
```

- Save & close file
- Create Windows Task Scheduler entry to run c:\update.bat weekly (with highest privilöeges)
- For the first run you need to execute update.bat with loal administrator's permission.
- Ypou can check the success by viewing the files in the "Logs" foldr.
